import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { UserCheckService } from '../user-check.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  {

  constructor(public router: Router, public userService: UserCheckService) { }

  model: any = {};

  onSubmit() {
    this.userService.setUser(true);
    this.router.navigateByUrl('/todo');
  }

}
