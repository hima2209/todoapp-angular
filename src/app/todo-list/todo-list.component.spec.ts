import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgForm } from '@angular/forms';
import { TodoListComponent } from './todo-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

describe('TodoListComponent', () => {
  let component: TodoListComponent;
  let fixture: ComponentFixture<TodoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, RouterTestingModule],
      declarations: [ TodoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should add items to array on calling addTodo', () => {
    component.addTodo('testForm');
    expect(component.todoArray.length).toBe(1);
  });

  it('should delete items to array on calling deleteItem', () => {
    component.addTodo('testForm');
    expect(component.todoArray.length).toBe(1);
    component.deleteItem('testForm');
    expect(component.todoArray.length).toBe(0);
  });

  it('should add items to array on calling todoSubmit', () => {
    component.todoSubmit('testForm');
    expect(component.todoArray.length).toBe(1);
  });

  it('should show alert with empty value calling todoSubmit', () => {
    spyOn(window, 'alert');
    component.todoSubmit('');
    expect(component.todoArray.length).toBe(0);
    expect(window.alert).toHaveBeenCalledWith('Field required **');
  });

  it('should show alert with empty value calling addTodo', () => {
    spyOn(window, 'alert');
    component.addTodo('');
    expect(component.todoArray.length).toBe(0);
    expect(window.alert).toHaveBeenCalledWith('Field required **');
  });

});
