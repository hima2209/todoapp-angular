import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder, FormControlName, Validators } from '@angular/forms';
import { UserCheckService } from '../user-check.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  constructor(public router: Router, public userService: UserCheckService) {}
  todoArray = [];
  todo;

  ngOnInit() {
    if (!this.userService.getUser()) {
      this.logout();
    }
  }

   addTodo(value) {
      if (value !== '') {
        this.todoArray.push(value);
      } else {
        alert('Field required **');
      }
   }

    /*delete item*/
    deleteItem(todo) {
        for (let i = 0 ; i <= this.todoArray.length ; i++) {
          if (todo === this.todoArray[i]) {
            this.todoArray.splice(i, 1);
          }
        }
    }

    // submit Form
    todoSubmit(value: any) {
      if (value !== '') {
      this.todoArray.push(value.todo);
      } else {
        alert('Field required **');
      }
    }

    logout() {
      this.userService.setUser(false);
      this.router.navigateByUrl('/login');
    }

}
