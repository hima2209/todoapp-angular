import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserCheckService {

  constructor() { }

  public user = false;

  setUser(value) {
    this.user = value;
  }

  getUser() {
    return this.user;
  }

}
